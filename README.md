# shopping-cart

Integrated Blockchain Shopping Cart API with invoice smart contracts.
```json
{

    source:"http://example.com/cart?id-sdfasd65sad666asdf",

    invoiceId: "4h1234gg3dk3uy",
    rows:[{
      "1" : {
        description:"Item One",
        amount:1.00,
        qty:10,
        total:10.00
        }}],
    total: 10.00,
    signature:[{SIGNATURE}]
}```