import { combineReducers } from 'redux';
import { reducer as FormReducer } from 'redux-form';
import AuthReducer from './authReducer';
import ModalsReducer from './modalsReducer';
import CarouselReducer from './carouselReducer';
import AlertsReducer from './alertsReducer';
import ErrorsReducer from './errorsReducer';
import CartReducer from './cartReducer';
import WishlistReducer from './wishlistReducer';
import ProductsReducer from './productsReducer';
import SpecialsReducer from './specialsReducer';
import RelatedProductsReducer from './relatedProductsReducer';
import FiltersReducer from './filtersReducer';
import SearchReducer from './searchReducer';

const rootReducer = combineReducers({
    form : FormReducer,
    alerts : AlertsReducer,
    auth : AuthReducer,
    carousels : CarouselReducer,
    /*cart : CartReducer,
    errors : ErrorsReducer,
    filters : FiltersReducer,
    modals : ModalsReducer,
    products : ProductsReducer,
    relatedProducts : RelatedProductsReducer,
    search : SearchReducer,
    specials : SpecialsReducer,
    wishlist : WishlistReducer,*/
});

export default rootReducer;
