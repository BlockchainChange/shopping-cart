import { ALERT_DANGER, ALERT_DEFAULT, ALERT_ERROR, ALERT_INFO, ALERT_PRIMARY, ALERT_SPECIAL, ALERT_SUCCESS, ALERT_WARNING } from '../actions';

export default (alert = null, action) => {
    switch (action.type) {
      case ALERT_DEFAULT:
      case ALERT_PRIMARY:
      case ALERT_INFO:
      case ALERT_SUCCESS:
      case ALERT_WARNING:
      case ALERT_ERROR:
      case ALERT_DANGER:
        return action.payload;
      default:
        return alert;
    }
};