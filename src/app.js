import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DefaultPage from './components/DefaultPage';
import Dashboard from './components/Dashboard';
import Notfound from './components/NotFound';
import RequireAuth from './components/Auth/RequireAuth';
import Header from './components/Header';
import { Container, Alert } from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alerts : { 
        default : {
          visible: true
        }
      }
    };
    this.onDismiss = this.onDismiss.bind(this);
  }
  
  onDismiss(name) {
    const alerts = this.state.alerts;
    alerts[name].visible = false;
    this.setState({ alerts });
  }
  
  componentDidMount(){
    setTimeout(this.onDismiss.bind(this, 'default'), 8000);
  }

  renderAlerts(type = 'danger', name='default', strong = <strong>Danger : </strong>, message = 'Danger will robinson!') {
    return <Alert className="mt-3" color={type} isOpen={this.state.alerts[name].visible} toggle={this.onDismiss.bind(this, name)}><h2>{strong} {message}</h2></Alert>;
  }

  render() {
    return (      
        <Container fluid className="px-0 mx-0  ">
          <Header />
          <Container>{this.renderAlerts()}</Container>
          <Router>
            <Switch>
              <Route component={DefaultPage} path="/" exact/> 
              <Route component={Notfound} /> 
            </Switch>
          </Router>
        </Container>      
    );
  }
}

const mapStateToProps = state => {
  return {
    authentication: state.authentication,
    modals: state.modals,
    carousels: state.carousels
  };
};

export default withRouter(connect(mapStateToProps)(App));