import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from './Dashboard';
import RequireAuth from './Auth/RequireAuth';
import Header from './Header';
import Footer from './Footer';
import Copyright from './Copyright'
import { toggleModal, getItems } from '../actions';
import { Container, Row, Col, UncontrolledCarousel} from 'reactstrap';
import { Alert, CardDeck, Card, CardBody, CardGroup, CardHeader, CardFooter, CardImg, CardLink, CardTitle,
  CardText, CardSubtitle, Media, Table, ButtonToolbar, Button, Progress } from 'reactstrap';

class DefaultPage extends Component {  
  componentDidMount(){
    this.props.getItems(this.props.carousels);
  }

  renderCarousel(name = 'DefaultCarousel', wrap=false) {
    if(wrap) {
      return <Container className="my-5"><UncontrolledCarousel items={this.props.carousels[name]}/></Container>     
    }
    return <UncontrolledCarousel items={this.props.carousels[name]} width="100px"/>
  }

  renderTrippleThumbnailProductSection(products, colors = ['success', 'info', 'success'], sectionBackground='bg-white') {
    return <section className={`py-3 ${sectionBackground}`}>
      <Container>
          <CardDeck>
              <Card sm={12} md={4} color={colors[0]}>
                  <CardHeader>
                      <CardTitle>Title</CardTitle>
                      <CardSubtitle>Some Subtitle</CardSubtitle>
                  </CardHeader>
                  <CardBody className="text-center">
                      <CardImg className="img-thumbnail" src="http://placehold.it/800x400/?text=Product 1" />
                      <CardText>Some Footer Text</CardText>
                  </CardBody>
                  <CardFooter>
                    <Progress multi>
                      <Progress striped color="info" bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped color="info" bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped color="info" bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <hr/>
                    <Button size="sm" color="success">Buy Now</Button>
                    <span className="float-right">
                      <Button size="sm" color="success">Add to Cart</Button>
                    </span>
                  </CardFooter>
              </Card>
              <Card sm={12} md={4} color={colors[1]}>
                  <CardHeader>
                      <CardTitle>Title</CardTitle>
                      <CardSubtitle>Some Subtitle</CardSubtitle>
                  </CardHeader>
                  <CardBody className="text-center">
                      <CardImg className="img-thumbnail" src="http://placehold.it/800x400/?text=Product 2" />
                      <CardText>Some Footer Text</CardText>
                  </CardBody>
                  <CardFooter>
                    <Progress multi>
                      <Progress striped bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped bar value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <hr/>
                    <Button size="sm" color="success">Buy Now</Button>
                    <span className="float-right">
                      <Button size="sm" color="success">Add to Cart</Button>
                    </span>
                  </CardFooter>
              </Card>
              <Card sm={12} md={4} color={colors[2]}>
                  <CardHeader>
                      <CardTitle>Title</CardTitle>
                      <CardSubtitle>Some Subtitle</CardSubtitle>
                  </CardHeader>
                  <CardBody className="text-center">
                      <CardImg className="img-thumbnail" src="http://placehold.it/800x400/?text=Product 3"/>
                      <CardText>Some Footer Text</CardText>
                  </CardBody>
                  <CardFooter>
                    <Progress multi>
                      <Progress striped bar color="info" value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped bar color="info" value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <Progress multi>
                      <Progress striped bar color="info" value="70" > SomeText</Progress>
                      <Progress bar color="success" value="30" >000000.000000</Progress>
                    </Progress>
                    <hr/>
                    <Button size="sm" color="success">Buy Now</Button>
                    <span className="float-right">
                      <Button size="sm" color="success">Add to Cart</Button>
                    </span>
                  </CardFooter>
              </Card>
          </CardDeck>
      </Container>
    </section>;
  }
  
  renderSingleProductSection(products, s) {
    const singleProductRow =  [
      <Col sm={12} md={4} className="align-self-center text-center">
          <img className="img-thumbnail" src="https://placehold.it/400x375" />
      </Col>,      
      <Col sm={12} md={8}>
            <Card style={{minHeight:'383px',maxHeight:'383px'}} bordered>
                <CardBody className="pb-3 px-0 py-0">
                    <CardTitle className="text-center pt-2">Some Product Title</CardTitle>
                    <hr/>
                    <CardSubtitle className="text-center pb-2">Some Product Subtitle</CardSubtitle>
                    <CardText className="mb-3 mx-2">Some table content about the products and details about the product.</CardText>
                    <Table hover className="mx-0 px-0 pt-0 pb-3">
                      <thead>
                          <tr>
                            <th>Head 1</th>
                            <th>Head 2 <span className="float-right"><ButtonToolbar><a className="badge badge-success" style={{cursor:'pointer'}} onClick={()=>{alert('Added to cart!')}}>Add To Cart</a></ButtonToolbar></span></th>
                          </tr>
                      </thead>
                      <tbody>
                        <tr>
                            <td>
                                Something
                            </td>
                            <td>
                                Else
                            </td>
                        </tr>                                  
                        <tr>
                            <td>
                                Something
                            </td>
                            <td>
                                Else
                            </td>
                        </tr>                                  
                        <tr>
                            <td>
                                Something
                            </td>
                            <td>
                                Else
                            </td>
                        </tr>
                      </tbody>
                      <tfoot>
                          <tr>
                              <td colSpan={2}>
                                  <small>some small text <span className="float-right"><sub className="badge badge-secondary" style={{cursor:'pointer'}} onClick={()=>{alert('View Cart?')}}>View Cart</sub></span></small>
                              </td>
                          </tr>
                      </tfoot>
                    </Table>
                    <br/>
                </CardBody>
            </Card>
      </Col>
    ];
    if(s) singleProductRow.reverse();
    return <section className="py-3">
        <Container>
          <Row>
              {singleProductRow}
          </Row>
        </Container>
    </section>;
  }

  renderTestominalSliderSection(products) {
    return '';
  }

  renderTrippleProductSection(products) {
    return <section className="py-5 bg-white">
      <Container>
        <Row>
          <Col sm={12} md={4}>
            <h3><strong> Title </strong></h3>
            <hr/>
            <Card color="white" className="px-3 py-3">
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
            </Card>
          </Col>
          <Col sm={12} md={4}>
            <h3><strong> Title </strong></h3>
            <hr/>
            <Card color="white" className="px-3 py-3">
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
            </Card>
          </Col>
          <Col sm={12} md={4}>
            <h3><strong> Title </strong></h3>
            <hr/>
            <Card color="white" className="px-3 py-3">
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
              <p>Some text that i typed out to fill this space for layout and design wireframes.</p>
            </Card>
          </Col>
        </Row>
      </Container>
    </section>;
  }

  render() {
    return (
          [this.renderCarousel(),
          <section className="py-5 bg-info">
            <Container>
              <Row className='content-align-center'>
                <Col className='text-center'>
                  <h1 className="display-4 ">Blockchain Shopping Cart</h1>
                </Col>
              </Row>
            </Container>
          </section>,
          this.renderSingleProductSection(this.props.products),
          <section className="py-5 bg-secondary">
            <Container>
              <Row className='content-align-center'>
                <Col className='text-center'>
                  <h2 className="display-4 text-primary">Brought to you by <a href="//blockchainchange.org/">blockchainchange.org</a></h2>
                </Col>
              </Row>
            </Container>
          </section>,
          this.renderTrippleThumbnailProductSection(this.props.products, ['primary','info','primary']),
          <section className="py-5 bg-secondary">
            <Container>
              <Row className='content-align-center'>
                <Col className='text-center'>
                  <h2 className="display-4 text-success">Buy & Sell Using Blockchain</h2>
                </Col>
              </Row>
            </Container>
          </section>,
          this.renderTrippleProductSection(this.props.products),         
          <section className="py-5 bg-info">
            <Container>
              <Row className='content-align-center'>
                <Col className='text-center'>
                  <h3 className="display-4">Dynamic Shopping Cart <hr className="bg-info"/> Realtime Product Management</h3>
                </Col>
              </Row>
            </Container>
          </section>,
          this.renderTrippleProductSection(this.props.products),        
          <section className="py-5 bg-secondary">
              <Container>
                  <Row className='content-align-center'>
                      <Col className='text-center'>
                          <h3 className="display-4 text-primary">Experience The Difference</h3>
                      </Col>
                  </Row>
              </Container>
          </section>,
          this.renderSingleProductSection(this.props.products, true ),         
          <section className="py-5 bg-secondary">
              <Container>
                  <Row className='content-align-center'>
                      <Col className='text-center'>
                          <h2 className="display-4 text-info">Real-time Shopping using Blockchain</h2>
                      </Col>
                  </Row>
              </Container>
          </section>,           
          this.renderTrippleThumbnailProductSection(this.props.products, ['info','primary','info'],'bg-white'),
          <section className="py-5 bg-info">
              <Container>
                  <Row className='content-align-center'>
                      <Col className='text-center'>
                          <h3 className="display-4 ">Find your transactions on the blockchain.</h3>
                      </Col>
                  </Row>
              </Container>
          </section>,            
          <Footer />]     
    );
  }
}

const mapStateToProps = state => {
  return {
    authentication: state.authentication,
    modals: state.modals,
    carousels: state.carousels
  };
};

export default withRouter(connect(mapStateToProps, {getItems})(DefaultPage));