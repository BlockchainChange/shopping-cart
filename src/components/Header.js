import React, {Component} from  'react';
import  { Collapse , Navbar , NavbarToggler,  NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    
    render() {
        return (<section>
            <Navbar className="bg-dark" expand="md">
                <NavbarBrand href="/" className="mr-auto">Blockchain</NavbarBrand>
                <span className="float-right"><NavbarToggler onClick={this.toggleNavbar} className="mr-2" /></span>
                <Collapse isOpen={!this.state.collapsed} navbar>
                <Nav className="color3 ml-auto" navbar>
                    <NavItem>
                        <NavLink href="/#View-Cart">View Cart</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="/#View-Cart">Sign In</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="https://bitbucket.org/BlockchainChange/Shopping-Cart" target="_default">Git</NavLink>
                    </NavItem>
                </Nav>
                </Collapse>
            </Navbar>
        </section>);
    }
};