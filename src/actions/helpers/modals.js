

export const modalToggle = (modal) => {
    return dispatch => {
        dispatch({
            type: MODAL_TOGGLE,
            payload:!modal.state || false
        });
    };
}
